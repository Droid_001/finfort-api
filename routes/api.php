<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaffMemberController;
use App\Http\Controllers\ProfessionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/staffMembers', [StaffMemberController::class, 'get_all']);
//Route::get('/staffMember/{id}', [StaffMemberController::class, 'get']);
Route::post('/staffMember', [StaffMemberController::class, 'create']);
Route::post('/staffMember/update', [StaffMemberController::class, 'update']);
Route::delete('/staffMember/{id}', [StaffMemberController::class, 'delete']);

Route::get('/professions', [ProfessionController::class, 'get_all']);
//Route::get('/profession/{id}', [ProfessionController::class, 'get']);
Route::post('/profession', [ProfessionController::class, 'create']);
Route::put('/profession', [ProfessionController::class, 'update']);
Route::delete('/profession/{id}', [ProfessionController::class, 'delete']);