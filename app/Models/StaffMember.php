<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class StaffMember extends Model
{
    use HasFactory;

    private static function setFullImagePath($path)
    {
        return url('storage/' . $path);
    }

    public static function get_all()
    {
        $staffMembers = StaffMember::All();

        foreach ($staffMembers as $i) {
            $i->img = StaffMember::setFullImagePath($i->img);
        }

        return $staffMembers;
    }

    public static function create($sm)
    {
        $staffMember = new StaffMember();
        $staffMember->name = $sm->input('name');
        $staffMember->surname = $sm->input('surname');
        $staffMember->date_of_birth = $sm->input('date_of_birth');
        $staffMember->profession_id = $sm->input('profession_id');
        if ($sm->hasFile('img')) {
            if ($sm->file('img')->isValid()) {
                $path = $sm->img->store('img/avatars');
                $staffMember->img = $path;
            }
        }
        $staffMember->save();
    }

    public static function updateStaff($sm)
    {
        
        $staffMember = StaffMember::findOrFail($sm->input('id'));
        $staffMember->name = $sm->input('name');
        $staffMember->surname = $sm->input('surname');
        $staffMember->date_of_birth = $sm->input('date_of_birth');
        $staffMember->profession_id = $sm->input('profession_id');
        if ($sm->hasFile('img')) {
            if ($sm->file('img')->isValid()) {

                if (Storage::has($staffMember->img)) {
                    Storage::delete($staffMember->img);
                }

                $path = $sm->img->store('img/avatars');
                $staffMember->img = $path;
            }
        }
        $staffMember->save();
    }

    public static function deleteStaff($id)
    {
        $staffMember = StaffMember::findOrFail($id);
        if (Storage::has($staffMember->img)) {
            Storage::delete($staffMember->img);
        }

        $staffMember->delete();
    }
}