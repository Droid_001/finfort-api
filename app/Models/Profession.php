<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    use HasFactory;

    public static function create($sm)
    {
        $profession = new Profession();
        $profession->name = $sm->input('name');
        $profession->save();
    }

    public static function updateStaff($sm)
    {

        $profession = Profession::findOrFail($sm->input('id'));
        $profession->name = $sm->input('name');

        $profession->save();
    }

    public static function deleteStaff($id)
    {
        $profession = Profession::findOrFail($id);
        $profession->delete();
    }
}
