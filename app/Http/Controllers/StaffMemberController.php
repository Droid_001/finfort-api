<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StaffMember;

class StaffMemberController extends Controller
{
    
    public function get_all(){
        return StaffMember::get_all();
    }

    public function create(Request $request){
        StaffMember::create($request);
    }

    public function update(Request $request){
        StaffMember::updateStaff($request);
    }

    public function delete(Request $request){
        StaffMember::deleteStaff($request->id);
    }
}
