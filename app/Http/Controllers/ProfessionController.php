<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profession;

class ProfessionController extends Controller
{
    public function get_all(){
        return Profession::All();
    }

    public function create(Request $request){
        Profession::create($request);
    }

    public function update(Request $request){
        Profession::updateStaff($request);
    }

    public function delete(Request $request){
        Profession::deleteStaff($request->id);
    }
}
